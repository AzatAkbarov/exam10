<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $this->record_id = $this->request->get($this->getIdParameter());
        $formMapper->add('full_name', TextType::class, [
            'required' => false
        ]);
        $formMapper->add('email', EmailType::class);
        $formMapper->add('username', TextType::class);
        if (empty($this->record_id)) {
            $formMapper->add('password', PasswordType::class);
            $formMapper->add('plainPassword', PasswordType::class);
        }
        $formMapper->add('enabled', CheckboxType::class, [
            'required' => null
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('email');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('username', null,['route'=> ['name'=>'show']]);
        $listMapper->add('enabled');
        $listMapper->add('roles');
        $listMapper->add('_action',null,array(
            'actions' => array(
                'show'=> array(),
                'delete'=> array()
            )
        ));
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show->add('username');
        $show->add('full_name');
        $show->add('email');
        $show->add('roles');
        $show->add('enabled');
    }
}
