<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\News;
use App\Entity\Rating;
use App\Entity\User;
use App\Form\CommentType;
use App\Form\FilterType;
use App\Form\NewsType;
use App\Form\PublishNewsType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $news = $this->getDoctrine()->getRepository(News::class)->findAllPublishedNews();
        $form = $this->createForm(FilterType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $category = $data['category'];
            $tag = $data['tags'];
            $news = $this->getDoctrine()->getRepository(News::class)->findAllPublishedNews($category,$tag);
            $paginator = $this->get('knp_paginator');
            $page = $request->query->getInt("page", 1);
            $resNews = $paginator->paginate(
                $news,
                $page,
                3
            );
            $form = $this->createForm(FilterType::class);
            return $this->render('index.html.twig', [
                'form' => $form->createView(),
                'news' => $resNews
            ]);
        }
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt("page", 1);
        $resNews = $paginator->paginate(
            $news,
            $page,
            3
        );
        return $this->render('index.html.twig', [
            'form' => $form->createView(),
            'news' => $resNews
        ]);
    }

    /**
     * @Route("/user/{id}/profile", name="user_profile")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function userProfileAction($id) {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $count = $user->getNews()->count();
        $allQuality = $this->getDoctrine()->getRepository(Rating::class)->getAllQualityAssessment($user);
        $allRelevance = $this->getDoctrine()->getRepository(Rating::class)->getAllRelevanceAssessment($user);

        $rating = ($count + $allQuality[1] + $allRelevance[1]) / 3;

        return $this->render('user/details.html.twig',[
            'user' => $user,
            'rating' => $rating
        ]);
    }

    /**
     * @Route("/news/{id}/details", name="news_details")
     * @param $id
     * @param Request $request
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newsDetailsAction($id, Request $request, ObjectManager $manager) {
        $news = $this->getDoctrine()->getRepository(News::class)->find($id);
        $comments = $news->getComments();
        $currentUser = $this->getUser();

        $quality = $this->getDoctrine()->getRepository(Rating::class)
            ->findRatingByCriteria($currentUser, $news, 1) ;
        $relevance = $this->getDoctrine()->getRepository(Rating::class)
            ->findRatingByCriteria($currentUser, $news, 2);
        $satisfaction = $this->getDoctrine()->getRepository(Rating::class)
            ->findRatingByCriteria($currentUser, $news, 3);
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if(!$currentUser) {
                return $this->redirect(
                    $request
                        ->headers
                        ->get('referer')
                );
            }
            $comment->setAuthor($currentUser)
                ->setNews($news);
            $manager->persist($comment);
            $manager->flush();
            return $this->redirect(
                $request
                    ->headers
                    ->get('referer')
            );
        }
        return $this->render('news/news_details.html.twig', [
            'news' => $news,
            'comments' => $comments,
            'count' => count($comments),
            'form' => $form->createView(),
            'quality' => $quality,
            'relevance' => $relevance,
            'satisfaction' => $satisfaction,
        ]);
    }

    /**
     * @Route("/user/news/{id}/{criterion}/{assessment}/rating", name="news_rating")
     * @param $id
     * @param Request $request
     * @param ObjectManager $manager
     * @param $criterion
     * @param $assessment
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newsRatingsAction($id, Request $request, ObjectManager $manager, $criterion, $assessment) {
        $news = $this->getDoctrine()->getRepository(News::class)->find($id);
        $currentUser = $this->getUser();

        $rating = new Rating();
        $rating->setUser($currentUser)
            ->setNews($news);
        switch ($criterion) {
            case 1:
                $rating->setQuality($assessment);
                break;
            case 2:
                $rating->setRelevance($assessment);
                break;
            case 3:
                $rating->setSatisfaction($assessment);
                break;
        }
        $manager->persist($rating);
        $manager->flush();

        return $this->redirect(
            $request
                ->headers
                ->get('referer')
        );
    }

    /**
     * @Route("/admin/comment/{id}/remove", name="remove_comment")
     * @param $id
     * @param ObjectManager $manager
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeCommentAction($id, ObjectManager $manager, Request $request) {
        $comment = $this->getDoctrine()->getRepository(Comment::class)->find($id);
        if($comment){
            $manager->remove($comment);
            $manager->flush();
        }
        return $this->redirect(
            $request
                ->headers
                ->get('referer')
        );
    }
    /**
     * @Route("/user/news/add", name="add_news")
     * @param Request $request
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addNewsAction(Request $request, ObjectManager $manager) {

        $news = new News();
        $currentUser = $this->getUser();
        $form = $this->createForm(NewsType::class, $news, ['attr' => ['var' => $currentUser->hasRole('ROLE_ADMIN')]]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $news->setAuthor($currentUser);
            $manager->persist($news);
            $manager->flush();
            return $this->redirectToRoute('homepage');
        }
        return $this->render('news/add_news.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/news/unpublished", name="unpublished_news")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function unpublishedNewsAction() {

        $news = $this->getDoctrine()->getRepository(News::class)->findAllUnpublishedNews();
        return $this->render('news/unpublished.html.twig', [
            'news' => $news
        ]);
    }

    /**
     * @Route("/admin/news/unpublished/{id}/publish", name="publish_news")
     * @param $id
     * @param Request $request
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function publishNewsAction($id, Request $request, ObjectManager $manager) {

        $news = $this->getDoctrine()->getRepository(News::class)->find($id);
        $form = $this->createForm(PublishNewsType::class, $news);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($news);
            $manager->flush();
            return $this->redirectToRoute('unpublished_news');
        }
        return $this->render('news/publish.html.twig', [
            'form' => $form->createView()
        ]);
    }
}