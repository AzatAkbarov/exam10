<?php


namespace App\Form;

use App\Entity\Category;
use App\Entity\Tag;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class NewsType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $var = $options['attr']['var'];

        $builder->add('title', TextType::class, [
            'label' => 'Введите название новости'
        ])
            ->add('description', TextareaType::class,[
                'label' => "Введите новость",
            ])
            ->add('category', EntityType::class,[
                'class' => Category::class,
                'choice_label' => 'categoryName',
                'label' => "Выберите категорию:",
            ])
            ->add('tags', EntityType::class,[
                'class' => Tag::class,
                'choice_label' => 'name',
                'label' => "Укажите тэги",
                'required'=> false,
                'multiple'=> true,
                'by_reference' => false
            ]);
                if($var) {
                    $builder->add('publishedDate');
                }
            $builder->add('save', SubmitType::class, [
                'label' => 'Сохранить'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'attr' => ['var'=> false]
        );
    }
    public function getBlockPrefix()
    {
        return 'app_bundle_object_type';
    }

}