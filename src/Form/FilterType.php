<?php


namespace App\Form;

use App\Entity\Category;
use App\Entity\Tag;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class FilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('category', EntityType::class,[
                'class' => Category::class,
                'choice_label' => 'categoryName',
                'label' => "Выберите категорию:",
                'placeholder' => 'Все категории',
                'required'=> false,
            ])
            ->add('tags', EntityType::class,[
                'class' => Tag::class,
                'choice_label' => 'name',
                'label' => "Выберите тег",
                'placeholder' => 'Все теги',
                'required'=> false,
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Поиск'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_object_type';
    }

}