<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{

    const USERNAME =
        [
            'azat',
            'admin',
            'aytmat',
            'maks',
            'urmat',
            'nurbek'
        ];
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $users_data = [
            'emails' => [
                'Азат Акбаров' => 'akbarov@gmail.com',
                'Админ Админов' => 'admin@gmail.com',
                'Айтмат Айтматов' => 'aytmat@gmail.com',
                'Максим Максимов' => 'maks@gmail.com',
                'Урмат Урматов' => 'urmat@gmail.com',
                'Нурбек Нурбеков' => 'nurbek@gmail.com',
            ]
        ];
        $i = 0;
        foreach ($users_data['emails'] as $key => $user_email) {
            $username = self::USERNAME[$i];
            $user = new User();
            $user
                ->setUsername($username)
                ->setFullName($key)
                ->setEnabled(true)
                ->setPlainPassword('12345678')
                ->setEmail($user_email);
            if ($users_data['emails'][$key] === 'admin@gmail.com') {
                $user->addRole('ROLE_ADMIN');
            }
            $manager->persist($user);
            $this->addReference(self::USERNAME[$i], $user);
            $i++;
        }
        $manager->flush();
    }
}