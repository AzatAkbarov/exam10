<?php

namespace App\DataFixtures;

use App\Entity\News;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class NewsFixtures extends Fixture implements DependentFixtureInterface
{

    const NEWS_NAME =
        [
            'Новость1',
            'Новость2',
            'Новость3',
            'Новость4',
        ];

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach (self::NEWS_NAME as $key => $new_name) {
            $news = new News();
            $news
                ->setTitle($new_name)
                ->setDescription('Bla-bla-bla')
                ->setAuthor($this->getReference(UserFixtures::USERNAME[0]))
                ->setCategory($this->getReference(CategoryFixtures::CATEGORY_NAME[0]))
                ->addTag($this->getReference(TagFixtures::TAGS_NAME[0]));
            $manager->persist($news);
            $this->getReference(TagFixtures::TAGS_NAME[0])->addNews($news);
            $this->addReference(self::NEWS_NAME[$key], $news);
        }
        $adminNews = new News();
        $adminNews
            ->setTitle('Новость Админа')
        ->setDescription('Bla-bla-bla')
        ->setAuthor($this->getReference(UserFixtures::USERNAME[1]))
        ->setCategory($this->getReference(CategoryFixtures::CATEGORY_NAME[1]))
        ->addTag($this->getReference(TagFixtures::TAGS_NAME[1]))
            ->setPublishedDate(new \DateTime);
        $manager->persist($adminNews);
        $this->getReference(TagFixtures::TAGS_NAME[1])->addNews($adminNews);
        $manager->flush();
    }

    function getDependencies()
    {
        return array(
            UserFixtures::class,
            CategoryFixtures::class,
            TagFixtures::class
        );
    }
}