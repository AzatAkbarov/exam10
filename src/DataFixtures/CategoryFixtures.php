<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{

    const CATEGORY_NAME =
        [
            'Политика',
            'Происшествия',
            'Некролог',
            'Прогноз погоды',
            'Городские истории',
        ];

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach (self::CATEGORY_NAME as $key => $category_name) {
            $category = new Category();
            $category->setCategoryName($category_name);
            $manager->persist($category);
            $this->addReference(self::CATEGORY_NAME[$key], $category);
        }
        $manager->flush();
    }
}