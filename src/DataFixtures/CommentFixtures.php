<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CommentFixtures extends Fixture implements DependentFixtureInterface
{

    const COMMENTS_NAME =
        [
            'так себе новость',
            'опять 25',
        ];

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach (self::COMMENTS_NAME as $comment_name) {
            $comment = new Comment();
            $comment->setComment($comment_name)
                ->setNews($this->getReference(NewsFixtures::NEWS_NAME[1]))
                ->setAuthor($this->getReference(UserFixtures::USERNAME[1]));
            $manager->persist($comment);
        }
        $manager->flush();
    }

    function getDependencies()
    {
        return array(
            UserFixtures::class,
            NewsFixtures::class
        );
    }
}