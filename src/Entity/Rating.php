<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="rating")
 * @ORM\Entity(repositoryClass="App\Repository\RatingRepository")
 */
class Rating
{
    const POSITIVE = 1;
    const NEGATIVE = -1;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quality;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $relevance;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $satisfaction;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="ratings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\News", inversedBy="ratings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $news;

    public function getId()
    {
        return $this->id;
    }

    public function getQuality(): ?int
    {
        return $this->quality;
    }

    public function setQuality($quality): self
    {
        $this->quality = $quality;

        return $this;
    }

    public function getRelevance()
    {
        return $this->relevance;
    }

    public function setRelevance($relevance): self
    {
        $this->relevance = $relevance;

        return $this;
    }

    public function getSatisfaction()
    {
        return $this->satisfaction;
    }

    public function setSatisfaction($satisfaction): self
    {
        $this->satisfaction = $satisfaction;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getNews(): ?News
    {
        return $this->news;
    }

    public function setNews(?News $news): self
    {
        $this->news = $news;

        return $this;
    }
}