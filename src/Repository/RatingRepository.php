<?php

namespace App\Repository;

use App\Entity\Rating;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Rating|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rating|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rating[]    findAll()
 * @method Rating[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RatingRepository extends ServiceEntityRepository
{

    public function findRatingByCriteria($user, $news, $criteria)
    {
        try {
            $qb = $this->createQueryBuilder('r')
                ->where('r.user =:user')
                ->andWhere('r.news =:news');
            switch ($criteria) {
                case 1:
                    $qb->andWhere('r.quality is NOT NULL ');
                    break;
                case 2:
                    $qb->andWhere('r.relevance is NOT NULL ');
                    break;
                case 3:
                    $qb->andWhere('r.satisfaction is NOT NULL ');
                    break;
            };
            return $qb
                ->setParameter('user', $user)
                ->setParameter('news', $news)
                ->getQuery()
                ->getOneOrNullResult();

        } catch (NonUniqueResultException $e) {
            return null;
        }

    }

    public function getAllQualityAssessment($user) {
        try {
            return $this->createQueryBuilder('r')
                ->select('SUM(r.quality)')
                ->where('r.user =:user')
                ->andWhere('r.quality is NOT NULL ')
                ->setParameter('user', $user)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }

    }
    public function getAllRelevanceAssessment($user) {
        try {
            return $this->createQueryBuilder('r')
                ->select('SUM(r.relevance)')
                ->where('r.user =:user')
                ->andWhere('r.quality is NOT NULL ')
                ->setParameter('user', $user)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }

    }

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Rating::class);
    }

}