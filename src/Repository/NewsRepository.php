<?php

namespace App\Repository;

use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository
{
    public function findAllPublishedNews($category = null, $tag = null)
    {
        $qb = $this->createQueryBuilder('n')
            ->where('n.publishedDate <= CURRENT_DATE()')
            ->andWhere('n.publishedDate is NOT NULL');
        if($category){
            $qb
                ->andWhere('n.category =:category')
                ->setParameter('category',$category);
        };
        if($tag){
            $qb->innerJoin('n.tags', 't')
                ->andWhere('t.id = :tag')
                ->setParameter('tag', $tag);
        };
        return $qb
            ->getQuery()
            ->getResult();
    }

    public function findAllUnpublishedNews()
    {
        return $this->createQueryBuilder('n')
            ->where('n.publishedDate >= CURRENT_DATE()')
            ->orWhere('n.publishedDate is NULL')
            ->getQuery()
            ->getResult();
    }


    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, News::class);
    }

}
